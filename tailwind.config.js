// tailwind.config.js
module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      "2xl": { min: "1535px" },
      // => @media (max-width: 1535px) { ... }
      xl: { max: "1280x" },
      // => @media (max-width: 1280x) { ... }
      lg: { max: "1024px" },
      // => @media (max-width: 1024px) { ... }
      xmd: { max: "801px" },
      // => @media (max-width: 801px) { ... }
      md: { max: "768px" },
      // => @media (max-width: 768px) { ... }
      sm: { max: "600px" },
      // => @media (max-width: 600px) { ... }
      xs: { max: "425px" },
      // => @media (max-width: 425px) { ... }
      xxs: { max: "375px" },
      // => @media (max-width: 375px) { ... }
      xss: { max: "360px" },
    },
    extend: {
      colors: {
        litesky: "#A8DEE9",
        darkgreen: "#079BBB",
        white: "#ffff",
        black: "#1A1A1A",
        orange: "#FCBC45",
        whiteghost: "#F9F9F9",
        darkorange: "#FF6810",
        primary: "#ffccae",
        litegray: "#7A7A7A",
        gray: "#555555",
        liteorange: "#ffe0a7",
        ghostewhite: "#F5F7FE",
        darkgray: "#333333",
        navyblue: "#0D243C",
      },
      spacing: {
        "10px": "10px",
        "20px": "20px",
        "34px": "34px",
        "40px": "40px",
        "43px": "43px",
        "45px": "45px",
        "48px": "48px",
        "50px": "50px",
        "56px": "56px",
        "53px": "53px",
        "70px": "70px",
        "77px": "77px",
        "82px": "82px",
        "99px": "99px",
        "110px": "110px",
        "115px": "115px",
        "126px": "126px",
        "129px": "129px",
        "131px": "131px",
        "139px": "139px",
        "150px": "150px",
        "181px": "181px",
        "188px": "188px",
        "202px": "202px",
        "232px": "232px",
        "238px": "238px",
        "250px": "250px",
        "257px": "257px",
        "269px": "269px",
        "310px": "310px",
        "340px": "340px",
        "360px": "360px",
        "394px": "394px",
        "435px": "435px",
        "445px": "445px",
        "491px": "491px",
        "504px": "504px",
        "552px": "552px",
        "573px": "573px",
        "592px": "592px",
        "643px": "643px",
        "1088px": "1088px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
