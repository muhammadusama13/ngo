import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Popularcard from "../components/popularcard/popularcard";
import Eventscard from "../components/eventscard/eventscard";
import { useRouter } from "next/router";

const Index = () => {
  const Prevarrow = ({ onClick }) => {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-prev"
        onClick={onClick}
      >
        <img src="images/left.svg" />
      </button>
    );
  };
  const Nextarrow = ({ onClick }) => {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-next"
        onClick={onClick}
      >
        <img src="images/right.svg" />
      </button>
    );
  };
  var silder1 = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    nextArrow: <Nextarrow />,
    prevArrow: <Prevarrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
        },
      },
    ],
  };
  const router = useRouter();
  return (
    <>
      {/* Start Banner Section */}
      <Slider {...silder1} className="slider1">
        <div className="banner_silder_item sm:px-20px">
          <div className="flex flex-col justify-center items-center h-full">
            <h1 className="text-white font40 fontlato text-center sm:text-3xl">
              It's not how much{" "}
              <span className="text-darkgreen"> we give </span> but how much{" "}
              <br />
              <span className="text-darkgreen"> love we put </span> into giving.
            </h1>
            <p className="text-center text-base text-white mt-20px sm:mt-5 sm:text-sm">
              Lorem Ipsum is simply dummy text of the printing and typesetting{" "}
              <br />
              industry. Lorem Ipsum has been the industry's standard dummy{" "}
              <br />
              text ever since the 1500s.
            </p>
            <div className="mt-20px flex">
              <button className="text-base bg-darkgreen rounded-3xl text-white text-center h-43px w-150px sm:w-126px ">
                Donate now
              </button>
              <button className=" ml-40px flex items-center text-white text-base sm:ml-3">
                <img src="images/wac.svg" className="mr-1" /> Watch video{" "}
              </button>
            </div>
          </div>
        </div>
        <div className="banner_silder_item sm:px-20px">
          <div className="flex flex-col justify-center items-center h-full">
            <h1 className="text-white font40 fontlato text-center sm:text-3xl">
              It's not how much{" "}
              <span className="text-darkgreen"> we give </span> but how much{" "}
              <br />
              <span className="text-darkgreen"> love we put </span> into giving.
            </h1>
            <p className="text-center text-base text-white mt-20px sm:mt-5 sm:text-sm">
              Lorem Ipsum is simply dummy text of the printing and typesetting{" "}
              <br />
              industry. Lorem Ipsum has been the industry's standard dummy{" "}
              <br />
              text ever since the 1500s.
            </p>
            <div className="mt-20px flex">
              <button className="text-base bg-darkgreen rounded-3xl text-white text-center h-43px w-150px sm:w-126px ">
                Donate now
              </button>
              <button className=" ml-40px flex items-center text-white text-base">
                <img src="images/wac.svg" className="mr-1" /> Watch video{" "}
              </button>
            </div>
          </div>
        </div>
      </Slider>
      {/* End Banner Section */}
      {/* Start About Section */}
      <section className="customcontainer mx-auto mt-82px about_section flex items-center sm:flex-wrap">
        <div className="w-1/2 about_text sm:w-full">
          <p className="text-gray text-base font-semibold">With your help</p>
          <h1 className="font38 text-black font-bold  mt-3">
            We{"’"}ve funded 1369 water projects for 0.5 million people around
            the world.
          </h1>
          <p className="text-gray text-sm mt-77px">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, when an unknown printer took a galley.
          </p>
          <button className="text-base bg-darkgreen rounded-3xl text-white h-43px w-188px mt-40px shadow-lg">
            Join us now
          </button>
        </div>
        <div className="w-1/2  pl-6 sm:w-full sm:pl-0">
          <img
            src="images/about1.png"
            className="relative left-32 z-10 lg:left-16 xmd:w-full xmd:static  rounded-2xl"
          />
          <img
            src="images/about2.png"
            className="relative -top-16 left-11 z-0  rounded-3xl  xmd:w-full xmd:static"
          />
        </div>
      </section>
      {/* End About Section */}
      {/* Start popular Section */}
      <section className="bg-whiteghost py-50px">
        <div className="customcontainer mx-auto">
          <div className="flex justify-between popular_header">
            <h1 className="font38 font-bold text-black">Popular Causes</h1>
            <button
              className="bg-transparent text-darkgreen text-sm font-semibold flex items-center"
              onClick={() => router.push("/causes")}
            >
              View all causes
              <img src="images/righta.svg" className="ml-2" />
            </button>
          </div>
          <div className="mt-45px flex sm:flex-wrap sm:flex-col-reverse">
            <div className="w-1/2 sm:w-full sm:mt-4">
              <img src="images/pop.png" className="w-full rounded-2xl" />
            </div>
            <div className="w-1/2 pl-5 sm:w-full sm:pl-0">
              <h1 className="font38 font-bold lineheight50 text-black">
                Providing access to safe water sanitation, and hygiene.
              </h1>
              <p className="text-sm text-darkgreen font-semibold mt-40px">
                $9125
              </p>
              <div className="bg-litesky h-1 mt-10px">
                <div className="bg-darkgreen w-4/12 h-full"></div>
              </div>
              <div className="flex justify-between py-0.5">
                <p className="text-sm font-semibold text-black">41% Donated</p>
                <p className="text-sm font-semibold text-black">
                  Goal:{""} $20,000
                </p>
              </div>
              <p className="text-gray text-sm mt-20px">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, when an unknown printer took a
                galley.
              </p>
              <button className="text-base bg-darkgreen rounded-3xl text-white h-45px w-188px mt-34px shadow-lg">
                Donate now
              </button>
            </div>
          </div>
          <div className="mt-11 flex justify-between sm:flex-wrap">
            <Popularcard
              title="Give them water"
              text="$5,300"
              textstyle="text-orange"
              progressstyle="bg-liteorange"
              progress="bg-orange w-1/2"
              donatetext="41% Donated"
              goaltext="Goal: $12,000"
              bodytext="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry."
              btnclass="text-orange"
              btntext="Donate now"
              btnimg="images/oran.svg"
            />
            <Popularcard
              title="Give them food"
              text="$5,300"
              textstyle="text-darkgreen"
              progressstyle="bg-litesky"
              progress="bg-darkgreen w-1/2"
              donatetext="41% Donated"
              goaltext="Goal: $12,000"
              bodytext="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry."
              btnclass="text-darkgreen"
              btntext="Donate now"
              btnimg="images/blue.svg"
            />
            <Popularcard
              title="Give them education"
              text="$5,300"
              textstyle="text-darkorange"
              progressstyle="bg-primary"
              progress="bg-darkorange w-1/2"
              donatetext="41% Donated"
              goaltext="Goal: $12,000"
              bodytext="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry."
              btnclass="text-darkorange"
              btntext="Donate now"
              btnimg="images/doran.svg"
            />
          </div>
        </div>
      </section>
      {/* End popular Section */}
      {/* Start upcoming events section*/}
      <section className="customcontainer mx-auto mt-20">
        <div className="flex justify-between items-center popular_header">
          <h1 className="font38 font-bold ">Upcoming event</h1>
          <button className="bg-transparent text-darkgreen text-sm font-semibold flex items-center">
            View all events
            <img src="images/blue.svg" className="ml-2" />
          </button>
        </div>
        <div className="mt-45px flex xmd:flex-wrap">
          <div className="w-1/2 xmd:w-full">
            <div className="relative">
              <img
                src="images/events.png"
                className="shadow-2xl rounded-xl xmd:w-full"
              />
              <h1 className="bg-ghostewhite h-82px w-150px rounded-xl text-center text-darkgreen py-5 absolute bottom-6 left-4 font26 font-bold">
                24 Dec
              </h1>
            </div>
            <h1 className="text-3xl font-bold text-black mt-8">
              Clean Water for All
            </h1>
            <div className="mt-7 flex items-center xs:flex-wrap">
              <div className="flex items-center ">
                <img src="images/time.svg" className="mr-5" />
                <p className="text-litegray text-lg">1:00 pm -</p>
                <p className="text-litegray text-lg"> 1:00 pm</p>
              </div>
              <div className="flex items-center ml-10 xs:mt3 xs:ml-0">
                <img src="images/location.svg" className="mr-5" />
                <p className="text-litegray text-lg">Los angles</p>
              </div>
            </div>
          </div>
          <div className="w-1/2 xmd:w-full">
            <Eventscard
              date="25 Dec"
              Eventname="One-Day Fundraiser"
              time="12:00 PM -  03:00 PM"
              place="New York"
              imgsrc="images/event1.png"
            />
            <Eventscard
              date="30 Dec"
              Eventname="Donate to Educate"
              time="12:00 PM -  03:00 PM"
              place="New York"
              imgsrc="images/event1.png"
            />
            <Eventscard
              date="12 Jan"
              Eventname="Education for All"
              time="12:00 PM -  03:00 PM"
              place="New York"
              imgsrc="images/event1.png"
            />
          </div>
        </div>
      </section>
      {/* End upcoming events section*/}
      {/* start could raise section*/}
      <section className="bg-darkgray py-77px mt-20">
        <div className="customcontainer mx-auto flex sm:flex-wrap sm:flex-col-reverse">
          <div className="w-1/2 sm:w-full">
            <img src="images/child.png" className="sm:w-full" />
          </div>
          <div className="w-1/2 could_raise  sm:w-full">
            <h1 className="font38 font-bold text-white">
              There's no limit to what you could raise!
            </h1>
            <p className="text-sm text-white mt-20">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, when an unknown printer took a
              galley.
            </p>
            <div className="flex flex-wrap justify-between mt-8">
              <div className=" flex items-center mb-8 w-1/2 xs:w-full">
                <img src="images/chl1.svg" />
                <div className="ml-2">
                  <h2 className="text-xl text-white font-semibold">
                    1,20,500+
                  </h2>
                  <p className="text-white text-base">Children rescued</p>
                </div>
              </div>
              <div className=" flex items-center mb-8 w-1/2 xs:w-full">
                <img src="images/chl1.svg" />
                <div className="ml-2">
                  <h2 className="text-xl text-white font-semibold">$52,000</h2>
                  <p className="text-white text-base">Total donations</p>
                </div>
              </div>
              <div className=" flex items-center mb-8 w-1/2 xs:w-full">
                <img src="images/chl3.svg" />
                <div className="ml-2">
                  <h2 className="text-xl text-white font-semibold">9,700</h2>
                  <p className="text-white text-base">Projects completed</p>
                </div>
              </div>
              <div className="flex items-center mb-8 w-1/2 xs:w-full">
                <img src="images/chl4.svg" />
                <div className="ml-2">
                  <h2 className="text-xl text-white font-semibold">1250</h2>
                  <p className="text-white text-base">Active volunteers</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* End could raise section*/}
      {/*Start Voter section section*/}
      <section className="voter_section">
        <div className="customcontainer mx-auto py-14 ">
          <div className="bg-white rounded-xl p-10 w-592px sm:w-full sm:p-4">
            <h1 className="text-black font32 font-bold">Become a volunteer</h1>
            <p className="text-litegray text-sm mt-7">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard.
            </p>
            <form className="mt-7 flex flex-wrap justify-between sm:flex-col">
              <div className="mb-8">
                <input
                  type="text"
                  placeholder="Full name"
                  className="w-238px h-45px text-litegray text-sm rounded pl-4 border border-litegray sm:w-full"
                />
              </div>
              <div className="mb-8">
                <input
                  type="email"
                  placeholder="Email"
                  className="w-238px h-45px text-litegray text-sm rounded pl-4 border border-litegray sm:w-full"
                />
              </div>
              <div className="mb-8">
                <select className="w-238px h-45px text-litegray text-sm rounded pl-4 border border-litegray sm:w-full">
                  <option>Country</option>
                </select>
              </div>
              <div className="mb-8">
                <input
                  type="text"
                  placeholder="Mobile number"
                  className="w-238px h-45px text-litegray text-sm rounded pl-4 border border-litegray sm:w-full"
                />
              </div>
              <div className="w-full">
                <textarea
                  className="resize-none w-full h-139px text-litegray text-sm rounded p-4 border border-litegray sm:w-full"
                  placeholder="Message"
                ></textarea>
              </div>
              <div>
                <button className="text-base bg-darkgreen rounded-3xl text-white h-45px w-188px mt-40px shadow-lg">
                  Apply now
                </button>
              </div>
            </form>
          </div>
        </div>
      </section>
      {/*End Voter section section*/}
      {/*Start organizations section*/}
      <section className="organization_section py-24 mb-10">
        <div className="customcontainer mx-auto flex xmd:flex-wrap">
          <div className="w-1/2 could_raise xmd:w-full">
            <h1 className="font38 font-bold text-black lineheight50">
              Trusted by the world biggest{" "}
              <span className="text-darkgreen">organizations. </span>
            </h1>
            <p className="text-litegray text-sm mt-20">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
            <button className="text-base bg-darkgreen rounded-3xl text-white h-45px w-188px mt-40px shadow-lg">
              Join us now
            </button>
          </div>
          <div className="flex justify-center items-center flex-col w-1/2 xmd:w-full sm:mt-4">
            <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg mb-6">
              <img src="images/bsg.svg" />
            </div>
            <div className="flex mb-6 xs:flex-col ">
              <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg">
                <img src="images/bsg2.svg" />
              </div>
              <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg mx-8 xs:mx-0 xs:my-3">
                <img src="images/bsg3.svg" />
              </div>
              <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg">
                <img src="images/bsg4.svg" />
              </div>
            </div>
            <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg mb-6">
              <img src="images/bsg5.svg" />
            </div>
          </div>
        </div>
      </section>
      {/*end organizations section*/}
    </>
  );
};

export default Index;
