import React from "react";

const Contact = () => {
  return (
    <>
      {/* Start banner section */}
      <div className="bg_banner bg_about flex flex-col justify-center items-center">
        <h1 className="text-white font38 font-bold relative z-50">
          Contact us
        </h1>
      </div>
      {/* End banner section */}
      <div className="customcontainer mx-auto mt-16 flex sm:flex-wrap">
        <div className="w-1/2 contact_section sm:w-full">
          <h1 className="text-black font38 font-bold">Get in touch</h1>
          <p className="text-litegray text-sm mt-8">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived.
          </p>
          <div className="mt-10 flex xs:flex-wrap">
            <div className="w-1/2 xs:w-full">
              <h4 className="text-black font-semibold text-xl">Los Angels</h4>
              <div className="flex items-center mt-4">
                <img src="images/mail.svg" />
                <div className="ml-10px">
                  <p className="text-litegray text-xs">Email</p>
                  <p className="text-litegray text-sm">Support@smile.org</p>
                </div>
              </div>
              <div className="flex items-center mt-4">
                <img src="images/call.svg" />
                <div className="ml-10px">
                  <p className="text-litegray text-xs">Phone</p>
                  <p className="text-litegray text-sm">+92 333 9053362</p>
                </div>
              </div>
              <div className="flex items-center mt-4">
                <img src="images/map.svg" />
                <div className="ml-10px">
                  <p className="text-litegray text-xs">Location</p>
                  <p className="text-litegray text-sm">Zero Point, New York.</p>
                </div>
              </div>
            </div>
            <div className="w-1/2 xs:w-full">
              <h4 className="text-black font-semibold text-xl">
                New York USA{" "}
              </h4>
              <div className="flex items-center mt-4">
                <img src="images/mail.svg" />
                <div className="ml-10px">
                  <p className="text-litegray text-xs">Email</p>
                  <p className="text-litegray text-sm">Support@smile.org</p>
                </div>
              </div>
              <div className="flex items-center mt-4">
                <img src="images/call.svg" />
                <div className="ml-10px">
                  <p className="text-litegray text-xs">Phone</p>
                  <p className="text-litegray text-sm">+1 345 6789</p>
                </div>
              </div>
              <div className="flex items-center mt-4">
                <img src="images/map.svg" />
                <div className="ml-10px">
                  <p className="text-litegray text-xs">Location</p>
                  <p className="text-litegray text-sm">Zero Point, New York</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="w-1/2 sm:w-full">
          <form className="boxshadow ml-auto w-491px px-56px py-40px rounded-lg relative top-36 bgwhite xmd:static xmd:w-full xmd:px-20px sm:mt-4">
            <div>
              <input
                type="text"
                placeholder="Full name"
                className="border border-litegray mb-7 h-50px w-full rounded-lg pl-5 text-litegray"
              />
            </div>
            <div>
              <input
                type="email"
                placeholder="Email"
                className="border border-litegray mb-7 h-50px w-full rounded-lg pl-5 text-litegray"
              />
            </div>
            <div>
              <select className="border border-litegray mb-7 h-50px w-full rounded-lg pl-5 text-litegray">
                <option>Country</option>
              </select>
            </div>
            <div>
              <textarea
                className="border border-litegray mb-7 h-139px w-full rounded-lg pl-5 py-3 text-litegray resize-none"
                placeholder="Message"
              />
            </div>
            <div className="">
              <button className="text-base bg-darkgreen rounded-3xl text-white h-45px w-188px  shadow-lg">
                Send message
              </button>
            </div>
          </form>
        </div>
      </div>
      <div className="mt-20">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14143117.941545919!2d60.323371148826894!3d30.068124090484666!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38db52d2f8fd751f%3A0x46b7a1f7e614925c!2sPakistan!5e0!3m2!1sen!2s!4v1638248321459!5m2!1sen!2s"
          width="100%"
          height="450"
          loading="lazy"
        ></iframe>
      </div>
    </>
  );
};

export default Contact;
