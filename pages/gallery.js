import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import SimpleReactLightbox, { SRLWrapper } from "simple-react-lightbox";

const Prevarrow = ({ onClick }) => {
  return (
    <button
      type="button"
      data-role="none"
      className="slick-arrow slick-prev"
      onClick={onClick}
    >
      <img src="images/gleft.svg" />
    </button>
  );
};
const Nextarrow = ({ onClick }) => {
  return (
    <button
      type="button"
      data-role="none"
      className="slick-arrow slick-next"
      onClick={onClick}
    >
      <img src="images/gright.svg" />
    </button>
  );
};

const Gallery = () => {
  var silder2 = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    nextArrow: <Nextarrow />,
    prevArrow: <Prevarrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
        },
      },
    ],
  };
  const options = {
    buttons: {
      showAutoplayButton: false,
      showCloseButton: true,
      showDownloadButton: false,
      showFullscreenButton: false,
      showNextButton: true,
      showPrevButton: true,
      showThumbnailsButton: false,
      size: "40px",
    },
  };

  return (
    <>
      {/* Start banner section */}
      <div className="bg_banner bg_about flex flex-col justify-center items-center">
        <h1 className="text-white font38 font-bold relative z-50">
          Our gallery{" "}
        </h1>
      </div>
      {/* End banner section */}
      {/* Start International volunteers */}
      <div className="mt-50px">
        <h1 className="text-black font38 font-bold text-center">
          International volunteers day
        </h1>
        <div className=" mt-50px ">
          <SimpleReactLightbox>
            <SRLWrapper options={options}>
              <div className="flex flex-wrap sm:flex-wrap">
                <div className="w-1/2 gallery_img sm:w-full">
                  <img
                    src="images/v1.png"
                    className="cursor-pointer w-full h-394px object-cover"
                  />
                </div>
                <div className="w-1/2 gallery_img sm:w-full">
                  <img
                    src="images/v2.png"
                    className="cursor-pointer w-full h-394px object-cover"
                  />
                </div>
                <div className="w-1/2 gallery_img sm:w-full">
                  <img
                    src="images/v3.png"
                    className="cursor-pointer w-full h-394px object-cover"
                  />
                </div>
                <div className="w-1/2 gallery_img sm:w-full">
                  <img
                    src="images/v4.png"
                    className="cursor-pointer w-full h-394px object-cover"
                  />
                </div>
              </div>
            </SRLWrapper>
          </SimpleReactLightbox>
        </div>
      </div>
      {/* End International volunteers */}
      {/* Start Campaign section */}
      <div className="customcontainer mx-auto mt-50px ">
        <h1 className="text-black font38 font-bold text-center">
          Campaign for homeless
        </h1>
        <Slider {...silder2} className="gallery_slider mt-50px">
          <div>
            <img src="images/gs.png" className="rounded-3xl  w-full" />
          </div>
          <div>
            <img src="images/gs.png" className="rounded-3xl  w-full" />
          </div>
        </Slider>
      </div>
      {/* End Campaign section */}
      {/* Start  become a volunteer*/}
      <section className="customcontainer mx-auto flex items-center mt-50px mb-44 sm:flex-wrap">
        <div className="w-1/2 sm:w-full">
          <h1 className="text-black font38 font-bold">
            Let's make a change <br /> become a volunteer
          </h1>
          <button className="text-base bg-darkgreen rounded-3xl text-white h-45px w-188px mt-34px shadow-lg">
            Join us now
          </button>
        </div>
        <div className="w-1/2 relative sm:w-full mt-8">
          <img src="images/cas.png" className=" rounded-3xl xmd:hidden" />
          <div className="bg-white w-360px min-h-269px  absolute top-44 shadow-2xl rounded-3xl -left-20 p-10  xmd:static sm:w-full">
            <div className="flex  items-center justify-between">
              <div className="flex   items-center">
                <img src="images/cust1.png " className="h-45px w-45px " />
                <p className="text-black font15 font-medium ml-2">Elizabet</p>
              </div>
              <img src="images/user1.svg" className="h-14 w-14" />
            </div>
            <h1 className="text-darkgray text-base font-semibold mt-4">
              Join with us !!
            </h1>
            <p className="text-litegray font15 mt-20px">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore
            </p>
          </div>
        </div>
      </section>
      {/* End  become a volunteer*/}
    </>
  );
};

export default Gallery;
