import React from "react";

const About = () => {
  return (
    <>
      {/* Start banner section */}
      <div className="bg_banner bg_about flex flex-col justify-center items-center">
        <h1 className="text-white font38 font-bold relative z-50">About us </h1>
      </div>
      {/* End banner section */}
      {/* Start Vedio section */}
      <div className="relative -top-20 sm:top-10 sm:w-full sm:px-20px">
        <img
          src="images/vpost.png"
          className="block w-552px mx-auto rounded-2xl"
        />
        <img
          src="images/play.svg"
          className="absolute left-1/2 top-139px cursor-pointer sm:left-188px sm:top-110px xs:left-40 xs:top-82px"
        />
      </div>
      {/* End Vedio section */}
      {/* Start Our-mission section */}
      <section className="customcontainer mx-auto">
        <h1 className="text-black font32 xmd:text-3xl font-bold text-center">
          We are non-profit organization that supports
          <span className="text-darkgreen">
            good causes <br />
          </span>
          and <span className="text-darkgreen"> positive change </span>
          especially
          <span className="text-darkgreen">
            water, food and education <br />
          </span>
          all over the world.
        </h1>
        <div className="flex justify-between mt-16 sm:flex-wrap">
          <div className="w-72 xmd:w-60 sm:mb-4 ">
            <h1 className="text-black font32 font-bold ">Our mission</h1>
            <p className="text-litegray text-sm mt-10px">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s.
            </p>
          </div>
          <div className="w-72 xmd:w-60 sm:mb-4">
            <h1 className="text-black font32 font-bold ">Our vision</h1>
            <p className="text-litegray text-sm mt-10px">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s.
            </p>
          </div>
          <div className="w-72 xmd:w-60 sm:mb-4">
            <h1 className="text-black font32 font-bold ">Our values</h1>
            <ul className="mt-10px">
              <li className="flex text-litegray text-sm">
                <img src="images/list.svg" className="mr-3" />
                <span>Accountability </span>
              </li>
              <li className="flex text-litegray text-sm">
                <img src="images/list.svg" className="mr-3" />

                <span>Reliability</span>
              </li>
              <li className="flex text-litegray text-sm">
                <img src="images/list.svg" className="mr-3" />

                <span> Cost-effectiveness</span>
              </li>
              <li className="flex text-litegray text-sm">
                <img src="images/list.svg" className="mr-3" />

                <span>Personal</span>
              </li>
              <li className="flex text-litegray text-sm">
                <img src="images/list.svg" className="mr-3" />
                <span>service</span>
              </li>
            </ul>
          </div>
        </div>
      </section>
      {/* Ens Our-mission section */}
      {/* Start Our-story section */}
      <section className="bg-whiteghost py-45px mt-16">
        <div className="customcontainer mx-auto">
          <h1 className="text-black font32 font-bold text-center">Our story</h1>
          <div className="flex mt-40px items-center sm:flex-wrap">
            <div className="w-1/2 sm:w-full">
              <img
                src="images/story.png"
                className="w-504px h-573px object-cover"
              />
            </div>
            <div className="w-1/2 lg:pl-5 sm:w-full sm:pl-0 sm:mt-6">
              <div className="mb-8">
                <h3 className="text-darkgreen font-bold text-xl ">2000</h3>
                <h1 className="text-black text-xl font-semibold mt-2.5">
                  The beginning of everything
                </h1>
                <p className="text-litegray text-sm mt-2.5">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam sed diam nonumy eirmod tempor
                  invidunt.
                </p>
              </div>
              <div className="mb-8">
                <h3 className="text-darkgreen font-bold text-xl ">2010</h3>
                <h1 className="text-black text-xl font-semibold mt-2.5">
                  Collected a donation of $52,000
                </h1>
                <p className="text-litegray text-sm mt-2.5">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam sed diam nonumy eirmod tempor
                  invidunt.
                </p>
              </div>
              <div className="mb-8">
                <h3 className="text-darkgreen font-bold text-xl ">2020</h3>
                <h1 className="text-black text-xl font-semibold mt-2.5">
                  We have 1250 volunteers
                </h1>
                <p className="text-litegray text-sm mt-2.5">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam sed diam nonumy eirmod tempor
                  invidunt.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* End Our-story section */}
      {/*Start organizations section*/}
      <section className="organization_section py-24 mb-10">
        <div className="customcontainer mx-auto flex xmd:flex-wrap">
          <div className="w-1/2 could_raise xmd:w-full">
            <h1 className="font38 font-bold text-black lineheight50">
              Trusted by the world biggest{" "}
              <span className="text-darkgreen">organizations. </span>
            </h1>
            <p className="text-litegray text-sm mt-20">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
            <button className="text-base bg-darkgreen rounded-3xl text-white h-45px w-188px mt-40px shadow-lg">
              Join us now
            </button>
          </div>
          <div className="flex justify-center items-center flex-col w-1/2 xmd:w-full sm:mt-4">
            <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg mb-6">
              <img src="images/bsg.svg" />
            </div>
            <div className="flex mb-6 xs:flex-col ">
              <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg">
                <img src="images/bsg2.svg" />
              </div>
              <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg mx-8 xs:mx-0 xs:my-3">
                <img src="images/bsg3.svg" />
              </div>
              <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg">
                <img src="images/bsg4.svg" />
              </div>
            </div>
            <div className="bgwhite rounded-full w-129px h-129px flex justify-center items-center shadow-lg mb-6">
              <img src="images/bsg5.svg" />
            </div>
          </div>
        </div>
      </section>
      {/*end organizations section*/}
    </>
  );
};

export default About;
