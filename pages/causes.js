import React from "react";
import Donatedcard from "../components/donatedcard/donatedcard";

const Causes = () => {
  return (
    <>
      {/* Start banner section */}
      <div className="bg_banner bg_about flex flex-col justify-center items-center">
        <h1 className="text-white font38 font-bold relative z-50">
          Causes list{" "}
        </h1>
      </div>
      {/* End banner section */}
      {/* Start Donated Card */}
      <section className="customcontainer mx-auto mt-50px grid grid-cols-3  gap-8 xmd:grid-cols-2 sm:grid-cols-1">
        <Donatedcard
          className="water_bg"
          title="Give them water"
          text="$5,300"
          donatetext="41% Donated"
          goaltext="Goal: $12,000"
          bodytext="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry."
        />
        <Donatedcard
          className="educate_bg"
          title=" Donate to educate"
          text="$5,300"
          donatetext="41% Donated"
          goaltext="Goal: $12,000"
          bodytext="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry."
        />
        <Donatedcard
          className="food_bg"
          title="Give them food"
          text="$5,300"
          donatetext="41% Donated"
          goaltext="Goal: $12,000"
          bodytext="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry."
        />
        <Donatedcard
          className="educate_bg"
          title=" Donate to educate"
          text="$5,300"
          donatetext="41% Donated"
          goaltext="Goal: $12,000"
          bodytext="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry."
        />
        <Donatedcard
          className="food_bg"
          title="Give them food"
          text="$5,300"
          donatetext="41% Donated"
          goaltext="Goal: $12,000"
          bodytext="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry."
        />
        <Donatedcard
          className="water_bg"
          title="Give them water"
          text="$5,300"
          donatetext="41% Donated"
          goaltext="Goal: $12,000"
          bodytext="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry."
        />
      </section>
      {/* End Donated Card */}

      {/* Start  become a volunteer*/}
      <section className="customcontainer mx-auto flex items-center mt-50px mb-44 sm:flex-wrap">
        <div className="w-1/2 sm:w-full">
          <h1 className="text-black font38 font-bold">
            Let's make a change <br /> become a volunteer
          </h1>
          <button className="text-base bg-darkgreen rounded-3xl text-white h-45px w-188px mt-34px shadow-lg">
            Join us now
          </button>
        </div>
        <div className="w-1/2 relative sm:w-full mt-8">
          <img src="images/cas.png" className=" rounded-3xl xmd:hidden" />
          <div className="bg-white w-360px min-h-269px  absolute top-44 shadow-2xl rounded-3xl -left-20 p-10  xmd:static sm:w-full">
            <div className="flex  items-center justify-between">
              <div className="flex   items-center">
                <img src="images/cust1.png " className="h-45px w-45px " />
                <p className="text-black font15 font-medium ml-2">Elizabet</p>
              </div>
              <img src="images/user1.svg" className="h-14 w-14" />
            </div>
            <h1 className="text-darkgray text-base font-semibold mt-4">
              Join with us !!
            </h1>
            <p className="text-litegray font15 mt-20px">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore
            </p>
          </div>
        </div>
      </section>
      {/* End  become a volunteer*/}
    </>
  );
};

export default Causes;
