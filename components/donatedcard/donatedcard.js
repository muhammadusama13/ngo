import React from "react";

const Donatedcard = ({
  className,
  title,
  text,
  donatetext,
  goaltext,
  bodytext,
}) => {
  return (
    <div
      className={` w-340px h-491px rounded-3xl shadow-lg overflow-hidden  ${className}`}
    >
      <div
        className={`h-full w-full   px-20px py-7 flex flex-col  justify-end mb-7 lg:w-310px xxs:w-full sm:mx-auto backgroundColor`}
      >
        <h2 className="text-white text-xl font-semibold">{title}</h2>
        <p className={`font-medium text-white text-sm mt-4 `}>{text}</p>
        <div className="mt-10px h-1 bg-litesky">
          <div className="h-full bg-darkgreen w-3/4"></div>
        </div>
        <div className="flex justify-between py-0.5">
          <p className="text-sm font-semibold text-white">{donatetext}</p>
          <p className="text-sm font-semibold text-white">{goaltext}</p>
        </div>
        <p className="text-white text-sm mt-4">{bodytext}</p>
        <button
          className={`text-sm mt-4 text-white bg-darkgreen h-40px w-150px rounded-full`}
        >
          Donate now
        </button>
      </div>
    </div>
  );
};

export default Donatedcard;
