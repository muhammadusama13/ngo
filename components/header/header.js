import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/dist/client/router";

const Header = () => {
  const [menu, setmenu] = useState(false);
  const route = useRouter();
  return (
    <>
      <header className="bg-darkgreen h-126px relative">
        <div className="mx-auto customcontainer flex justify-between items-center h-full">
          <div>
            <Link href="/">
              <img src="images/logo.svg" className="cursor-pointer" />
            </Link>
          </div>
          <div className="flex items-center xmd:hidden">
            <img src="images/mail.svg" />
            <div className="ml-2">
              <h5 className="text-xs text-white font-medium">Email</h5>
              <p className="text-sm text-white font-medium">
                Support@smile.org
              </p>
            </div>
          </div>
          <div className="flex items-center xmd:hidden">
            <img src="images/call.svg" />
            <div className="ml-2">
              <h5 className="text-xs text-white font-medium">Phone</h5>
              <p className="text-sm text-white font-medium">+1 2345 6789</p>
            </div>
          </div>
          <div className="flex items-center xmd:hidden">
            <img src="images/map.svg" />
            <div className="ml-2">
              <h5 className="text-xs text-white">Location</h5>
              <p className="text-sm text-white font-medium">
                Zero Point, New York
              </p>
            </div>
          </div>
          <div className="xmd:hidden">
            <button className="w-131px h-43px bg-orange text-sm text-black rounded-3xl font-semibold">
              Donate now
            </button>
          </div>
          <button className="hidden xmd:block" onClick={() => setmenu(!menu)}>
            <img src="images/bars.svg" className="h-9 w-9" />
          </button>
        </div>
        <div
          className={`${
            menu ? "menu_show" : "menu_hide"
          } nav__bar customcontainer mx-auto rounded bg-white transition-all duration-300 h-16 flex justify-between items-center px-5 z-10 
        lg:px-40px sm:px-20px xmd:fixed xmd:top-0 xmd:left-0 xmd:h-full xmd:z-50 xmd:items-start xmd:flex-col xmd:justify-start xmd:rounded-none xmd:overflow-y-scroll
        `}
        >
          <div className="hidden  xmd:block xmd:w-full mb-4">
            <button
              className="ml-auto my-5 block h-9 w-9"
              onClick={() => setmenu(!menu)}
            >
              <img src="images/cross.svg" className=" h-full w-full" />
            </button>
            <button className="w-131px h-43px bg-orange text-sm text-black rounded-3xl font-medium xmd:w-full ">
              Donate now
            </button>
          </div>
          <ul className="flex xmd:flex-col">
            <li
              className={` ${
                route.pathname == "/" ? "text-orange" : ""
              } text-sm font-semibold text-black pr-14 hover:text-orange xmd:py-4`}
            >
              <Link href="/">Home</Link>
            </li>
            <li
              className={` ${
                route.pathname == "/about" ? "text-orange" : ""
              } text-sm font-semibold text-black pr-14 hover:text-orange xmd:py-4`}
            >
              <Link href="/about">About</Link>
            </li>
            <li
              className={` ${
                route.pathname == "/gallery" ? "text-orange" : ""
              } text-sm font-semibold text-black pr-14 hover:text-orange xmd:py-4`}
            >
              <Link href="/gallery">Gallery</Link>
            </li>
            <li
              className={` ${
                route.pathname == "/causes" ? "text-orange" : ""
              } text-sm font-semibold text-black pr-14 hover:text-orange xmd:py-4`}
            >
              <Link href="/causes">Causes</Link>
            </li>
            <li
              className={` ${
                route.pathname == "/contact" ? "text-orange" : ""
              } text-sm font-semibold text-black pr-14 hover:text-orange xmd:py-4`}
            >
              <Link href="/contact">Contact us</Link>
            </li>
          </ul>
          <div className="hidden xmd:block">
            <div className="flex items-center my-4">
              <img src="images/mail.svg" />
              <div className="ml-2">
                <h5 className="text-xs">Email</h5>
                <p className="text-sm font-semibold">Support@helpo.org</p>
              </div>
            </div>
            <div className="flex items-center my-4">
              <img src="images/call.svg" />
              <div className="ml-2">
                <h5 className="text-xs">Phone</h5>
                <p className="text-sm font-medium">+92 333 9053362</p>
              </div>
            </div>
            <div className="flex items-center my-4">
              <img src="images/map.svg" />
              <div className="ml-2">
                <h5 className="text-xs">Location</h5>
                <p className="text-sm font-medium">University road, Peshawar</p>
              </div>
            </div>
          </div>
          <div className="flex items-center xmd:hidden">
            <p className="text-litegray text-sm pr-4 font-semibold">
              Follow us:
            </p>
            <div className="flex items-center header_soical">
              <Link href="https://www.facebook.com/">
                <div>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="6.701"
                    height="12.607"
                    viewBox="0 0 6.701 12.607"
                  >
                    <path
                      id="Path_42"
                      data-name="Path 42"
                      d="M70.944,49.874l.347-2.264H69.119V46.14A1.132,1.132,0,0,1,70.4,44.917h.988V42.989a12.044,12.044,0,0,0-1.753-.153,2.764,2.764,0,0,0-2.959,3.048v1.726H64.682v2.264h1.989v5.474a7.914,7.914,0,0,0,2.448,0V49.874Z"
                      transform="translate(-64.682 -42.836)"
                    />
                  </svg>
                </div>
              </Link>
              <Link href="https://www.instagram.com/">
                <div>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="12.363"
                    height="12.363"
                    viewBox="0 0 12.363 12.363"
                  >
                    <g
                      id="Group_16"
                      data-name="Group 16"
                      transform="translate(-582.99 595.272)"
                    >
                      <path
                        id="Path_38"
                        data-name="Path 38"
                        d="M455.407,68.324a.725.725,0,1,0,.725.725.725.725,0,0,0-.725-.725"
                        transform="translate(137.038 -661.388)"
                      />
                      <path
                        id="Path_39"
                        data-name="Path 39"
                        d="M400.088,77.991a3.045,3.045,0,1,0,3.045,3.045,3.048,3.048,0,0,0-3.045-3.045m0,4.995a1.95,1.95,0,1,1,1.95-1.95,1.952,1.952,0,0,1-1.95,1.95"
                        transform="translate(189.134 -670.126)"
                      />
                      <path
                        id="Path_40"
                        data-name="Path 40"
                        d="M372.541,57.729h-4.936a3.718,3.718,0,0,1-3.714-3.713V49.08a3.718,3.718,0,0,1,3.714-3.714h4.936a3.718,3.718,0,0,1,3.714,3.714v4.936a3.718,3.718,0,0,1-3.714,3.713m-4.936-11.2a2.553,2.553,0,0,0-2.55,2.55v4.936a2.553,2.553,0,0,0,2.55,2.55h4.936a2.553,2.553,0,0,0,2.551-2.55V49.08a2.553,2.553,0,0,0-2.551-2.55Z"
                        transform="translate(219.098 -640.638)"
                      />
                    </g>
                  </svg>
                </div>
              </Link>
              <Link href="https://twitter.com/">
                <div>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="11.5"
                    height="9.465"
                    viewBox="0 0 11.5 9.465"
                  >
                    <path
                      id="Path_37"
                      data-name="Path 37"
                      d="M699.743,61.569a4.749,4.749,0,0,1-1.163.332c.187-.031.462-.37.572-.507a2.132,2.132,0,0,0,.386-.7c.01-.02.017-.045,0-.06a.066.066,0,0,0-.062.005,5.932,5.932,0,0,1-1.379.528.092.092,0,0,1-.1-.025,1.108,1.108,0,0,0-.12-.123,2.441,2.441,0,0,0-.668-.409,2.326,2.326,0,0,0-1.024-.165,2.449,2.449,0,0,0-.972.275,2.5,2.5,0,0,0-.784.64,2.409,2.409,0,0,0-.468.938,2.54,2.54,0,0,0-.025.99c.008.055,0,.063-.048.055a7.365,7.365,0,0,1-4.738-2.412c-.055-.063-.085-.063-.131,0a2.4,2.4,0,0,0,.409,2.839c.093.088.188.176.291.256a2.414,2.414,0,0,1-.914-.256c-.055-.035-.083-.015-.088.048a1.451,1.451,0,0,0,.015.271,2.43,2.43,0,0,0,1.5,1.937,1.414,1.414,0,0,0,.3.093,2.705,2.705,0,0,1-.9.028c-.065-.012-.09.02-.065.083a2.522,2.522,0,0,0,1.884,1.578c.085.015.171.015.256.035-.005.008-.01.008-.015.015a2.985,2.985,0,0,1-1.287.682,4.61,4.61,0,0,1-1.954.25c-.105-.015-.127-.014-.156,0s0,.043.03.07c.133.088.269.166.407.241a6.453,6.453,0,0,0,1.306.523,6.957,6.957,0,0,0,6.755-1.579,7.088,7.088,0,0,0,1.869-5.171c0-.074.088-.115.14-.155a4.565,4.565,0,0,0,.919-.956.29.29,0,0,0,.061-.183v-.01c0-.03,0-.021-.047,0"
                      transform="translate(-688.29 -60.435)"
                      fill="#fcbc45"
                    />
                  </svg>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
