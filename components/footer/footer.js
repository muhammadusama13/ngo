import React from "react";
import Link from "next/link";

const Footer = () => {
  return (
    <footer className="bg-navyblue">
      <section className="customcontainer mx-auto flex flex-wrap py-24">
        <div className="w-1/4 xmd:w-1/2 xmd:mb-8 sm:w-full ">
          <img src="images/logo.svg" />
          <p className="text-base text-white mt-8">
            we are a non-profit organization engaged in the humanitarian sector
          </p>
          <div className="flex mt-9">
            <img src="images/ffb.svg" className="mr-3" />
            <img src="images/fins.svg" className="mr-3" />
            <img src="images/ftiw.svg" className="mr-3" />
          </div>
        </div>
        <div className="w-1/4 xmd:w-1/2 xmd:mb-8 sm:w-full ">
          <h4 className="text-white font19 "> LINKS</h4>
          <ul className="mt-8">
            <li className="text-base text-white pb-2 hover:text-orange">
              <Link href="/">ABOUT</Link>
            </li>
            <li className="text-base text-white pb-2 hover:text-orange">
              <Link href="/">VOLUNTEERS</Link>
            </li>
            <li className="text-base text-white pb-2 hover:text-orange">
              <Link href="/">GALLERY</Link>
            </li>
            <li className="text-base text-white pb-2 hover:text-orange">
              <Link href="/">EVENT</Link>
            </li>
            <li className="text-base text-white pb-2 hover:text-orange">
              <Link href="/">FAQ</Link>
            </li>
          </ul>
        </div>
        <div className="w-1/4 xmd:w-1/2 xmd:mb-8 sm:w-full ">
          <h4 className="text-white font19 "> CONTACT</h4>
          <ul className="mt-8">
            <li className="text-white text-base pb-2">
              Office # F12-17 new <br /> Gaga Mall, zero point
            </li>
            <li className="text-white text-base flex pb-2">
              <img src="images/call.svg" className="mr-4" />
              <span>+1 2345 6789</span>
            </li>
            <li className="text-white text-base flex pb-2">
              <img src="images/mail.svg" className="mr-4" />
              <span>charity@helpo.org</span>
            </li>
          </ul>
        </div>
        <div className="w-1/4 xmd:w-1/2 xmd:mb-8 sm:w-full ">
          <h4 className="text-white font19 ">SUBSCRIBE</h4>
          <p className="text-white text-base mt-8">
            we will inform you about the important things of our donations and
            events
          </p>
          <form className="mt-5">
            <div className="relative">
              <input
                type="text"
                placeholder="Your email"
                className="w-full h-45px pl-12 rounded-lg"
              />
              <button className="w-full h-45px rounded-lg font-semibold text-black font15 bg-orange mt-5">
                Subscribe
              </button>
              <img
                src="images/email.svg"
                className="absolute top-3.5 left-3 "
              />
            </div>
          </form>
        </div>
      </section>
      <div className=" border-t border-litegray py-9">
        <div className="flex justify-between customcontainer mx-auto">
          <p className="text-white text-sm">
            © 2020 Charity.com. All Right Reserved
          </p>
          <p className="text-white text-sm">HELP & SUPPORT | PRIVACY POLICY</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
