import React from "react";

const Eventscard = ({ place, time, date, Eventname, imgsrc }) => {
  return (
    <div className="events_cards flex px-4 py-6 rounded-2xl xs:px-2 xs:py-3 xs:mt-4">
      <div className="  w-3/4 ">
        <h1 className="text-xl font-semibold text-darkgreen">{date}</h1>
        <h2 className="text-xl text-black font-bold mt-3">{Eventname}</h2>
        <div className="flex items-center mt-4 xs:flex-wrap">
          <div className="flex items-center ">
            <svg
              className="mr-2"
              xmlns="http://www.w3.org/2000/svg"
              width="20.481"
              height="20.481"
              viewBox="0 0 20.481 20.481"
            >
              <path
                id="clock_time_loading"
                data-name="clock, time, loading"
                d="M11.241,1A10.241,10.241,0,1,0,21.481,11.241,10.241,10.241,0,0,0,11.241,1Zm0,18.619a8.379,8.379,0,1,1,8.379-8.379A8.379,8.379,0,0,1,11.241,19.619Zm5.586-8.379a.931.931,0,0,1-.931.931H11.241a.931.931,0,0,1-.931-.931V5.655a.931.931,0,1,1,1.862,0V10.31H15.9A.931.931,0,0,1,16.826,11.241Z"
                transform="translate(-1 -1)"
              />
            </svg>
            <p className="text-litegray text-lg">{time}</p>
          </div>
          <div className="flex items-center ml-10 xs:mt3 xs:ml-0">
            <svg
              className="mr-3"
              xmlns="http://www.w3.org/2000/svg"
              width="16.741"
              height="20.514"
              viewBox="0 0 16.741 20.514"
            >
              <path
                id="location"
                d="M19.75,9.358A8.37,8.37,0,1,0,3.01,9.313,12.987,12.987,0,0,0,10.992,21.4a.931.931,0,0,0,.387.083.942.942,0,0,0,.39-.084A12.985,12.985,0,0,0,19.75,9.358ZM11.379,19.524A10.9,10.9,0,0,1,4.87,9.376a6.509,6.509,0,0,1,13.018,0v.061a10.892,10.892,0,0,1-6.509,10.087Zm0-13.871A3.724,3.724,0,1,0,15.1,9.376,3.724,3.724,0,0,0,11.379,5.652Zm0,5.586a1.862,1.862,0,1,1,1.862-1.862A1.862,1.862,0,0,1,11.379,11.238Z"
                transform="translate(-3.01 -0.965)"
              />
            </svg>
            <p className="text-litegray text-lg">{place}</p>
          </div>
        </div>
      </div>
      <div className="w-1/4">
        <img
          src={imgsrc}
          className="h-115px w-126px object-contain  rounded-xl"
        />
      </div>
    </div>
  );
};

export default Eventscard;
