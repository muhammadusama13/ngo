import React from "react";

const Popularcard = ({
  textstyle,
  text,
  title,
  progressstyle,
  progress,
  donatetext,
  goaltext,
  bodytext,
  btnclass,
  btntext,
  btnimg,
}) => {
  return (
    <div className="w-310px xmd:w-238px sm:w-full sm:mb-5 ">
      <h1 className="text-black text-xl font-bold">{title}</h1>
      <p className={`font-medium text-sm mt-4 ${textstyle}`}>{text}</p>
      <div className={`${progressstyle} mt-10px h-1`}>
        <div className={`h-full ${progress}`}></div>
      </div>
      <div className="flex justify-between py-0.5">
        <p className="text-sm font-semibold text-black">{donatetext}</p>
        <p className="text-sm font-semibold text-black">{goaltext}</p>
      </div>
      <p className="text-litegray text-sm mt-4">{bodytext}</p>
      <button
        className={`text-sm font-semibold flex  items-center mt-4 ${btnclass}`}
      >
        {btntext}
        <img src={btnimg} className="ml-2" />
      </button>
    </div>
  );
};

export default Popularcard;
